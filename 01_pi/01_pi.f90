program main
    implicit none
    integer(4)::n
    real(8)::pi    

    call input_param(n)
    call calc_pi_number(n,pi)
    call output_pi_number(n,pi)
contains
    subroutine input_param(n)   
        implicit none
        integer(4)::n !試行回数
        open(10,file="input.dat",status="old")
        read(10,*)n
        close(10)
    end subroutine input_param

    subroutine calc_pi_number(n,pi)    
        implicit none
        real(8)::pi
        integer(4)::i,n,n_in
        do i=1,n
            if(is_inside())n_in = n_in + 1
        enddo
        pi= 4.0*dble(n_in)/dble(n)
    end subroutine calc_pi_number

    function is_inside()
        implicit none
        real(8)::pos(2),l2
        logical::is_inside
        call random_number(pos) !(x,y)座標をランダムに
        l2 = dsqrt(pos(1)*pos(1)+pos(2)*pos(2))
        is_inside=.false.
        if(l2<1.0)is_inside=.true.
    end function is_inside

    subroutine output_pi_number(n,pi)    
        implicit none
        integer(4)::n
        real(8)::pi
        open(10,file="output.dat",status="replace")
        write(10,*)"n",n
        write(10,*)"pi",pi
        close(10)
    end subroutine output_pi_number

end program main